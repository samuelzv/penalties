export interface Team {
  name: string;
}

export interface Score {
  goals: number;
  executedPenalties: number;
}

export interface TeamScore {
  team: Team;
  score: Score;
}

export interface GameScore {
  teamScoreA: TeamScore;
  teamScoreB: TeamScore;
}


export interface PenaltyExecutionResult {
  team: Team;
  penaltyNumber: number;
  scored: boolean;
}