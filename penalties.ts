import * as rx from 'rxjs';
import { Team, GameScore, TeamScore, Score, PenaltyExecutionResult } from './interfaces';


const teamA: Team = {
  name: 'América',
};

const teamB: Team = {
  name: 'Cruz Azul',
};

const MAX_NUM_PENALTIES = 5;

const play = {
  executePenalty: () => true || Math.random() >= 0.5,
  getFreshScore: (teamA, teamB): GameScore => {
    const teamScoreA: TeamScore = {team: teamA, score: { goals: 0, executedPenalties: 0 }};
    const teamScoreB: TeamScore = {team: teamB, score: { goals: 0, executedPenalties: 0 }};
    return {
      teamScoreA,
      teamScoreB
    }
  },
  printGameScore: (gameScore: GameScore) => {
    const {name: nameTeamA} = gameScore.teamScoreA.team;
    const {name: nameTeamB} = gameScore.teamScoreB.team;
    const {goals: goalsTeamA} = gameScore.teamScoreA.score;
    const {goals: goalsTeamB} = gameScore.teamScoreB.score;

    const score = `Penalty:${gameScore.teamScoreA.score.executedPenalties} ${nameTeamA}:${goalsTeamA} vs ${nameTeamB}:${goalsTeamB}`;
    console.log(score);
  },
  determineWinnerTeam(gameScore: GameScore): TeamScore {
    const {score: teamScoreA} = gameScore.teamScoreA;
    const {score: teamScoreB} = gameScore.teamScoreB;

    if (teamScoreA.goals === teamScoreB.goals) {
      return null;
    }

    const possibleLooser: TeamScore = teamScoreA.goals > teamScoreB.goals ? gameScore.teamScoreB : gameScore.teamScoreA;
    const possibleWinner: TeamScore = possibleLooser === gameScore.teamScoreA ? gameScore.teamScoreB : gameScore.teamScoreA;

    const pendingPenalties = MAX_NUM_PENALTIES - possibleLooser.score.executedPenalties;
    if ( (possibleLooser.score.goals + pendingPenalties) < possibleWinner.score.goals) {
      return possibleWinner;
    }

    return null;
  }
};



const executePenalty$ = (team: Team) => {
  return rx.Observable.interval(1000)
    .map((index) => index + 1)
    .map((penaltyNumber: number): PenaltyExecutionResult => {
      return {
        team,
        penaltyNumber,
        scored: play.executePenalty()
      }
    })
    .take(MAX_NUM_PENALTIES);
};

const updateScore = (accumulator: GameScore, penaltyExecutionResult: PenaltyExecutionResult[]) => {
  const [penaltyExecutionResultA, penaltyExecutionResultB] = penaltyExecutionResult;

  const getNewTeamScore = (currentScore: Score,  penaltyExecutionResult: PenaltyExecutionResult): Score => {
    return {
        goals: currentScore.goals += penaltyExecutionResult.scored ? 1 : 0,
        executedPenalties:  penaltyExecutionResult.penaltyNumber
    }
  };
  const {score: teamScoreA} = accumulator.teamScoreA;
  const {score: teamScoreB} = accumulator.teamScoreB;

  accumulator.teamScoreA.score = getNewTeamScore(teamScoreA, penaltyExecutionResultA);
  accumulator.teamScoreB.score = getNewTeamScore(teamScoreB, penaltyExecutionResultB);

  return accumulator;
};


const penaltyResultA$ = executePenalty$(teamA);
const penaltyResultB$ = executePenalty$(teamB);

const playScore = play.getFreshScore(teamA, teamB)

const play$ = rx.Observable.zip(penaltyResultA$, penaltyResultB$)
                 .scan(updateScore, playScore)
                 .takeWhile((gameScore: GameScore) => play.determineWinnerTeam(gameScore) === null);


const fanObserver = {
  next: (currentGameScore: GameScore) => {
    play.printGameScore(currentGameScore);
  },
  error: (err)  => console.log('Error', err),
  complete: () => {
    console.log('Game over3');
    console.log(playScore);
  }
};

play$.subscribe(fanObserver);


