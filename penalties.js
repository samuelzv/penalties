"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rx = require("rxjs");
const teamA = {
    name: 'América',
};
const teamB = {
    name: 'Cruz Azul',
};
const MAX_NUM_PENALTIES = 5;
const play = {
    executePenalty: () => true || Math.random() >= 0.5,
    getFreshScore: (teamA, teamB) => {
        const teamScoreA = { team: teamA, score: { goals: 0, executedPenalties: 0 } };
        const teamScoreB = { team: teamB, score: { goals: 0, executedPenalties: 0 } };
        return {
            teamScoreA,
            teamScoreB
        };
    },
    printGameScore: (gameScore) => {
        const { name: nameTeamA } = gameScore.teamScoreA.team;
        const { name: nameTeamB } = gameScore.teamScoreB.team;
        const { goals: goalsTeamA } = gameScore.teamScoreA.score;
        const { goals: goalsTeamB } = gameScore.teamScoreB.score;
        const score = `Penalty:${gameScore.teamScoreA.score.executedPenalties} ${nameTeamA}:${goalsTeamA} vs ${nameTeamB}:${goalsTeamB}`;
        console.log(score);
    },
    determineWinnerTeam(gameScore) {
        const { score: teamScoreA } = gameScore.teamScoreA;
        const { score: teamScoreB } = gameScore.teamScoreB;
        if (teamScoreA.goals === teamScoreB.goals) {
            return null;
        }
        const possibleLooser = teamScoreA.goals > teamScoreB.goals ? gameScore.teamScoreB : gameScore.teamScoreA;
        const possibleWinner = possibleLooser === gameScore.teamScoreA ? gameScore.teamScoreB : gameScore.teamScoreA;
        const pendingPenalties = MAX_NUM_PENALTIES - possibleLooser.score.executedPenalties;
        if ((possibleLooser.score.goals + pendingPenalties) < possibleWinner.score.goals) {
            return possibleWinner;
        }
        return null;
    }
};
const executePenalty$ = (team) => {
    return rx.Observable.interval(1000)
        .map((index) => index + 1)
        .map((penaltyNumber) => {
        return {
            team,
            penaltyNumber,
            scored: play.executePenalty()
        };
    })
        .take(MAX_NUM_PENALTIES);
};
const updateScore = (accumulator, penaltyExecutionResult) => {
    const [penaltyExecutionResultA, penaltyExecutionResultB] = penaltyExecutionResult;
    const getNewTeamScore = (currentScore, penaltyExecutionResult) => {
        return {
            goals: currentScore.goals += penaltyExecutionResult.scored ? 1 : 0,
            executedPenalties: penaltyExecutionResult.penaltyNumber
        };
    };
    const { score: teamScoreA } = accumulator.teamScoreA;
    const { score: teamScoreB } = accumulator.teamScoreB;
    accumulator.teamScoreA.score = getNewTeamScore(teamScoreA, penaltyExecutionResultA);
    accumulator.teamScoreB.score = getNewTeamScore(teamScoreB, penaltyExecutionResultB);
    return accumulator;
};
const penaltyResultA$ = executePenalty$(teamA);
const penaltyResultB$ = executePenalty$(teamB);
const playScore = play.getFreshScore(teamA, teamB);
const play$ = rx.Observable.zip(penaltyResultA$, penaltyResultB$)
    .scan(updateScore, playScore)
    .takeWhile((gameScore) => play.determineWinnerTeam(gameScore) === null);
const fanObserver = {
    next: (currentGameScore) => {
        play.printGameScore(currentGameScore);
    },
    error: (err) => console.log('Error', err),
    complete: () => {
        console.log('Game over3');
        console.log(playScore);
    }
};
play$.subscribe(fanObserver);
//# sourceMappingURL=penalties.js.map